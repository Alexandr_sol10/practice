﻿#include <stdio.h>
#include <locale.h>
/**
* @file practice_1c.cpp
* @author Сололматін О.С., гр. 515А
* @date 20.06.2019
* @brief Практична работа
*
* Задание 1
* Проектирование и разработка программы на языке С.Использование Git.*/

/**
* @param num - введеная цифра
* @param flag - для различия часов от минут
*/
int func_time(int num, int flag); 

int main()
{
	setlocale(LC_ALL, "Ukrainian");
	int time1, time2;
	printf("Введiть час: ");
	scanf_s("%d %d", &time1, &time2);

	if (time1 >= 24 || time1 < 0 || time2 >= 60 || time2 < 0) {
		printf("Час був введен нокорректно\n");
		return 0;
	}

	if (time1 < 10 && time2 < 10)
		printf("0%d 0%d -> ", time1, time2);
	else if (time1 < 10)
		printf("0%d %d -> ", time1, time2);
	else if (time2 < 10)
		printf("%d 0%d -> ", time1, time2);
	else
		printf("%d %d -> ", time1, time2);

	int flag = 0;
	flag = func_time(time1, flag);
	flag = func_time(time2, flag);
	return 0;
}

int func_time(int num, int flag)//вывод цифр словами
{

	const char *num1[] = { "нуль ", "одна ", "двi ", "три ", "чотири ", "п'ять ", "шiсть ", "сiм ", "вiсiм ", "дев'ять " };
	const char *num2[] = { "десять", "одинадацять", "дванадцять", "тринадцять", "чотирнадцять",	"п'ятнадцять", "шiстнадцять", "сiмнадцять", "вiсiмнадцять",	"дев'ятнадцять" };
	const char *num3[] = { "десять", "двадцять", "тридцять", "сорок","п'ятдесят" };
	const char *hour[] = { "годин", "години", "годин" };// строковый массив для склонения часов
	const char *min[] = { "хвилина", "хвилини", "хвилин" };// строковый массив для склонения минут

	if (num >= 20)
		printf("%s %s", num3[num / 10 - 1], num1[num % 10]);
	else
	{
		if (num > 9) printf("%s ", num2[num - 10]);
		else printf("%s ", num1[num]);
	}
	if (flag == 0)
	{
		if ((num >= 10 && num <= 20) || num % 10 == 0 || (num % 10 >= 5 && num % 10 <= 9))
			printf("%s ", hour[2]);
		else if (num % 10 == 1)
			printf("%s ", hour[0]);
		else printf("%s ", hour[1]);
		return 1;
	}
	else
	{
		if ((num >= 10 && num <= 20) || num % 10 == 0 || (num % 10 >= 5 && num % 10 <= 9))
			printf("%s ", min[2]);
		else if (num % 10 == 1)
			printf("%s ", min[0]);
		else printf("%s ", min[1]);
		return 1;
	}
}